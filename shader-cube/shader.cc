// g++ min.cc -lglut -lGL -lGLU -o min && ./min
// N=shader; g++ $N.cc loader.cc -lglut -lGL -lGLU -o $N && ./$N

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <GL/freeglut.h>

#include <iostream>

unsigned int load_shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr);

void drawCube();
void drawCubeL(int len);
void drawCubeL(int len, int showFaces[]);

// #define on / #undef off
#define DO_SHADER
#define VERT_SHADER "vertex.c"
#define FRAG_SHADER "frag.c"
#define CUBES_LEN 8

GLuint shaderProgram;

float rx=0, ry=0, rz=0;

void printInfo(){
  char *name;
  GLint active_attribs, max_length;

  glGetProgramiv(shaderProgram, GL_ACTIVE_ATTRIBUTES, &active_attribs);
  glGetProgramiv(shaderProgram, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &max_length);

  name = (char *)malloc(max_length + 1);

  for (unsigned i = 0; i < active_attribs; i++) {
      GLint size;
      GLenum type;

      glGetActiveAttrib(shaderProgram, i, max_length + 1, NULL,
                        &size, &type, name);
      //printf("%s %s is at location %d\n", get_name_of_GLSL_type(type),
      printf("%s %s is at location %d\n", "X",
             name, glGetAttribLocation(shaderProgram, name));
  }
  free(name);
}

void draw()
{
  int elapsedTime = glutGet(GLUT_ELAPSED_TIME);
  std::cout << elapsedTime << " ms" << std::endl;

  int vertexColorLocation = glGetUniformLocation(shaderProgram, "time");
  glUniform1f(vertexColorLocation, elapsedTime);

  // Set Background Color
  //glClearColor(0.2, 0.4, 0.4, 1.0);
  glClearColor(0., 0., 0., 1.0);
  // Clear screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Reset transformations
  glLoadIdentity();

  //glUseProgram(shaderProgram);

  //glTranslatef(.2, 0, 0);
  //glTranslatef(0, -0.8, 0);
  //glTranslatef(-0.8, 0,0);
  //glTranslatef(0,0,-0.9);

  rx+=0.5;
  ry+=0.9;
  rz+=0.75;
  //std::cout << rx << " " << ry << " " << rz << std::endl;
  glRotatef(rx, 1, 0, 0 );
  glRotatef(ry, 0, 1, 0 );
  glRotatef(rz, 0, 0, 1 );

  /*glPushMatrix();
    float sc = 0.4;
    glScalef(sc,sc,sc);
    drawCube();
  glPopMatrix();

  glPushMatrix();
    glTranslatef(0.7, 0, 0);
    sc = 0.15;
    glScalef(sc,sc,sc);
    drawCube();
  glPopMatrix();

  glPushMatrix();
    glTranslatef(0, 0.7, 0);
    sc = 0.08;
    glScalef(sc,sc,sc);
    drawCube();
  glPopMatrix();*/

  float sc;
  int fs[] = {1,1,1,1,1,1};

  fs[0]=0;
  fs[1]=0;
  fs[2]=0;
  fs[3]=0;
    glPushMatrix();
      sc = 0.03;
      glScalef(sc,sc,sc);
      drawCubeL(CUBES_LEN,fs);
    glPopMatrix();

    glPushMatrix();
      glTranslatef(0.5, 0., 0.);
      //glRotatef(elapsedTime/20., 1., 0., 0.);
      sc = 0.05;
      //fs[0]=0;
      //fs[1]=0;
      glScalef(sc,sc,sc);
      drawCubeL(CUBES_LEN,fs);
    glPopMatrix();

    glPushMatrix();
      //glRotatef(elapsedTime/20., 0., 1., 0.);
      glTranslatef(0., 0.5, 0.);
      sc = 0.04;
      //fs[2]=0;
      //fs[3]=0;
      glScalef(sc,sc,sc);
      drawCubeL(CUBES_LEN,fs);
    glPopMatrix();

    glPushMatrix();
      glRotatef(elapsedTime/10., 0., 1., 0.);
      glTranslatef(0., -0.6, 0.);
      sc = 0.02;
      //fs[2]=0;
      //fs[3]=0;
      glScalef(sc,sc,sc);
      drawCubeL(CUBES_LEN,fs);
    glPopMatrix();

  glFlush();
  glutSwapBuffers();

  // va esto??
  glutPostRedisplay();
}

int main(int argc, char **argv)
{
  // Initialize GLUT and process user parameters
  glutInit(&argc, argv);

  // Request double buffered true color window with Z-buffer
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  glutInitWindowSize(700,700);
  glutInitWindowPosition(100, 100);

  // Create window
  glutCreateWindow("Linux Journal OpenGL Cube");

  // Enable Z-buffer depth test
  glEnable(GL_DEPTH_TEST);

  // Callback functions
  glutDisplayFunc(draw);

  #ifdef DO_SHADER
    shaderProgram = load_shader(VERT_SHADER, FRAG_SHADER);
    //GLuint shaderProgram = load_shader(VERT_SHADER, FRAG_SHADER);
  	glUseProgram(shaderProgram);
    int uniformLocation = glGetUniformLocation(shaderProgram, "len");
    glUniform1f(uniformLocation, CUBES_LEN);
    printInfo();
  #endif

    // Get the location of the attributes that enters in the vertex shader
    /*GLint position_attribute = glGetAttribLocation(shaderProgram, "position");
    // Specify how the data for position can be accessed
    glVertexAttribPointer(position_attribute, 2, GL_FLOAT, GL_FALSE, 0, 0);
    // Enable the attribute
    glEnableVertexAttribArray(position_attribute);*/

  // Pass control to GLUT for events
  glutMainLoop();

  return 0;
}
