uniform float time;
uniform float len;

varying vec4 pos;

void main (void)
{
   gl_FragColor = vec4(
     pos.x/len,
     pos.y/len * time/9000.,
     pos.z/len,
     1.0
   );
}
