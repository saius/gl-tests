
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

void drawCube()
{
  // Render a cube
  glBegin( GL_QUADS );
  // Top face
  glColor3f(   0.0f, 1.0f,  0.0f );  // Green
  glVertex3f(  1.0f, 1.0f, -1.0f );  // Top-right of top face
  glVertex3f( -1.0f, 1.0f, -1.0f );  // Top-left of top face
  glVertex3f( -1.0f, 1.0f,  1.0f );  // Bottom-left of top face
  glVertex3f(  1.0f, 1.0f,  1.0f );  // Bottom-right of top face
  // Bottom face
  glColor3f(   1.0f,  0.5f,  0.0f ); // Orange
  glVertex3f(  1.0f, -1.0f, -1.0f ); // Top-right of bottom face
  glVertex3f( -1.0f, -1.0f, -1.0f ); // Top-left of bottom face
  glVertex3f( -1.0f, -1.0f,  1.0f ); // Bottom-left of bottom face
  glVertex3f(  1.0f, -1.0f,  1.0f ); // Bottom-right of bottom face
  // Front face
  glColor3f(   1.0f,  0.0f, 0.0f );  // Red
  glVertex3f(  1.0f,  1.0f, 1.0f );  // Top-Right of front face
  glVertex3f( -1.0f,  1.0f, 1.0f );  // Top-left of front face
  glVertex3f( -1.0f, -1.0f, 1.0f );  // Bottom-left of front face
  glVertex3f(  1.0f, -1.0f, 1.0f );  // Bottom-right of front face
  // Back face
  glColor3f(   1.0f,  1.0f,  0.0f ); // Yellow
  glVertex3f(  1.0f, -1.0f, -1.0f ); // Bottom-Left of back face
  glVertex3f( -1.0f, -1.0f, -1.0f ); // Bottom-Right of back face
  glVertex3f( -1.0f,  1.0f, -1.0f ); // Top-Right of back face
  glVertex3f(  1.0f,  1.0f, -1.0f ); // Top-Left of back face
  // Left face
  glColor3f(   0.0f,  0.0f,  1.0f);  // Blue
  glVertex3f( -1.0f,  1.0f,  1.0f);  // Top-Right of left face
  glVertex3f( -1.0f,  1.0f, -1.0f);  // Top-Left of left face
  glVertex3f( -1.0f, -1.0f, -1.0f);  // Bottom-Left of left face
  glVertex3f( -1.0f, -1.0f,  1.0f);  // Bottom-Right of left face
  // Right face
  glColor3f(   1.0f,  0.0f,  1.0f);  // Violet
  glVertex3f(  1.0f,  1.0f,  1.0f);  // Top-Right of left face
  glVertex3f(  1.0f,  1.0f, -1.0f);  // Top-Left of left face
  glVertex3f(  1.0f, -1.0f, -1.0f);  // Bottom-Left of left face
  glVertex3f(  1.0f, -1.0f,  1.0f);  // Bottom-Right of left face
  glEnd();
}

void drawCubeL(int len, int showFaces[])
{
  float centerOffset = len/2.;
  glPushMatrix();
  glTranslatef(-centerOffset, -centerOffset, -centerOffset);

  // Render a cube
  glBegin( GL_QUADS );
  // Bottom face
  if (showFaces[0]){
    glColor3f( 1.0f,  0.5f,  0.0f ); // Orange
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        glVertex3f( i,j,0);
        glVertex3f( i+1,j,0);
        glVertex3f( i+1,j+1,0);
        glVertex3f( i,j+1,0);
      }
    }
  }
  // Top face
  if (showFaces[1]){
    glColor3f( 0.0f, 1.0f,  0.0f );  // Green
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        glVertex3f( i,j,len);
        glVertex3f( i+1,j,len);
        glVertex3f( i+1,j+1,len);
        glVertex3f( i,j+1,len);
      }
    }
  }
  // Front face
  if (showFaces[2]){
    glColor3f( 1.0f,  0.0f, 0.0f );  // Red
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        glVertex3f(j,len,i);
        glVertex3f(j+1,len,i);
        glVertex3f(j+1,len,i+1);
        glVertex3f(j,len,i+1);
      }
    }
  }
  // Back face
  if (showFaces[3]){
    glColor3f(   1.0f,  1.0f,  0.0f ); // Yellow
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        glVertex3f(j,0,i);
        glVertex3f(j+1,0,i);
        glVertex3f(j+1,0,i+1);
        glVertex3f(j,0,i+1);
      }
    }
  }
  // Left face
  if (showFaces[4]){
    glColor3f(   0.0f,  0.0f,  1.0f);  // Blue
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        glVertex3f(0,j,i);
        glVertex3f(0,j+1,i);
        glVertex3f(0,j+1,i+1);
        glVertex3f(0,j,i+1);
      }
    }
  }
  // Right face
  if (showFaces[5]){
    glColor3f(   1.0f,  0.0f,  1.0f);  // Violet
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        glVertex3f(len,j,i);
        glVertex3f(len,j+1,i);
        glVertex3f(len,j+1,i+1);
        glVertex3f(len,j,i+1);
      }
    }
  }
  glEnd();
  glPopMatrix();
}

void drawCubeL(int len)
{
  int showFaces[] = {1,1,1,1,1,1};
  drawCubeL(len, showFaces);
}
