
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <vector>
#include <iostream>

//https://solarianprogrammer.com/2013/05/13/opengl-101-drawing-primitives/
// Compile a shader
GLuint load_and_compile_shader(const char *fname, GLenum shaderType) {
  // Load a shader from an external file
  std::vector<char> buffer;
  read_shader_src(fname, buffer);
  const char *src = &buffer[0];

  // Compile the shader
  GLuint shader = glCreateShader(shaderType);
  glShaderSource(shader, 1, &src, NULL);
  glCompileShader(shader);
  // Check the result of the compilation
  GLint test;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &test);
  if(!test) {
  	std::cerr << "Shader compilation failed with this message:" << std::endl;
  	std::vector<char> compilation_log(512);
  	glGetShaderInfoLog(shader, compilation_log.size(), NULL, &compilation_log[0]);
  	std::cerr << &compilation_log[0] << std::endl;
  	//glfwTerminate();
    glutLeaveMainLoop();
  	exit(-1);
  }
  return shader;
}

// Create a program from two shaders
GLuint create_program(const char *path_vert_shader, const char *path_frag_shader) {
	// Load and compile the vertex and fragment shaders
	GLuint vertexShader = load_and_compile_shader(path_vert_shader, GL_VERTEX_SHADER);
	GLuint fragmentShader = load_and_compile_shader(path_frag_shader, GL_FRAGMENT_SHADER);

	// Attach the above shader to a program
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	// Flag the shaders for deletion
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// Link and use the program
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	return shaderProgram;
}
