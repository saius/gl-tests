uniform float time;
uniform float len;

varying vec4 pos;

void main(void)
{
   vec4 a = gl_Vertex;
   //a.x = a.x * 0.5;
   //a.y = a.y + sin(a.x*time/1000.);
   a.x = a.x + 2.*sin((a.y/len)*3.1416*2.+time/100.);
   //asd = vec3(1,2,3);
   pos = gl_Vertex;
   gl_Position = gl_ModelViewProjectionMatrix * a;
   // ?? gl_Position =  mvp * position;
}
