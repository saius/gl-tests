## OPENGL TESTS

```
cd originals
g++ triangle.cc -lglut  -lGL -o triangle
g++ cube.cc -lglut  -lGL -o cube
g++ Random_Point.cpp -lglut -lGL -lGLU -o Random_Point
g++ rotateCube.cc -lglut -lGL -lGLU -o rotateCube
```

(and ./run output files)

`freeglut3 freeglut3-dev libglew-dev`

There are many free OpenGL implementations for Linux, but you need only one.   
I installed FreeGLUT, because it is the most up to date.   
FreeGLUT is an open-source alternative to the OpenGL Utility Toolkit (GLUT) library.   

- https://www.linuxjournal.com/content/introduction-opengl-programming
- https://www.includehelp.com/linux/how-to-install-opengl-in-ubuntu-linux.aspx
- https://www.3dgep.com/rendering-primitives-with-opengl/

### Shaders
- GLSL v1.10 (#110) ref https://mew.cx/glsl_quickref.pdf
- https://learnopengl.com/Getting-started/Shaders
   - https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/shader.h
- https://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/loading.php
- https://solarianprogrammer.com/2013/05/13/opengl-101-drawing-primitives/
- https://flyx.github.io/OpenGLAda/gl-attributes.html
- https://dokipen.com/modern-opengl-part-5-feeding-vertex-data-to-shaders/
- https://stackoverflow.com/questions/21601692/setting-up-a-mvp-matrix-in-opengl#21614967

### Keyboard
- http://www.swiftless.com/tutorials/opengl/keyboard.html

### Makefile
- https://www.softwaretestinghelp.com/cpp-makefile-tutorial/
- https://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile
- https://www.partow.net/programming/makefile/index.html

### GLM
OpenGL Mathematics (GLM) is a header only C++ mathematics library for graphics software based on the OpenGL Shading Language (GLSL) specifications.

GLM provides classes and functions designed and implemented with the same naming conventions and functionality than GLSL so that anyone who knows GLSL, can use GLM as well in C++.
- oficial https://github.com/g-truc/glm
- releases https://github.com/g-truc/glm/releases
- docs https://github.com/g-truc/glm/blob/master/manual.md#-1-getting-started
- tuto install https://rizaldijs.wordpress.com/2017/10/28/setting-up-glfw-glad-with-clion-on-ubuntu/

### GLFW
GLFW is an Open Source, multi-platform library for OpenGL, OpenGL ES and Vulkan development on the desktop. It provides a simple API for creating windows, contexts and surfaces, receiving input and events.

Requires glad2 for loading OpenGL and Vulkan functions.

- repo https://github.com/glfw/glfw
- site https://www.glfw.org/
- tuto https://learnopengl.com/Getting-started/Creating-a-window
- glad https://github.com/Dav1dde/glad
- generador de librería glad https://gen.glad.sh (usar opengl 3.3, core, y con tick en generate loader)
- full docs https://www.glfw.org/docs/latest/
- oficial example code https://www.glfw.org/documentation.html
- next: función de render/update inteligente https://stackoverflow.com/questions/20390028/c-using-glfwgettime-for-a-fixed-time-step

### OBJ loading
- https://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
- https://assimp-docs.readthedocs.io/en/latest/usage/use_the_lib.html#introduction
- assimp https://www.assimp.org/
