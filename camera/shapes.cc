
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

void drawAxis(){
  // https://www.khronos.org/opengl/wiki/Primitive
  glBegin(GL_LINES);
  glColor3f(1,0,0);
  glVertex3f(0,0,0);
  glVertex3f(1,0,0);

  glColor3f(0,1,0);
  glVertex3f(0,0,0);
  glVertex3f(0,1,0);

  glColor3f(0,0,1);
  glVertex3f(0,0,0);
  glVertex3f(0,0,1);
  glEnd();
}
