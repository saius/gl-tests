// https://www.khronos.org/opengl/wiki/Vertex_Shader
#version 400

layout(location = 0) in vec3 vertexpos;

// https://www.khronos.org/opengl/wiki/Uniform_(GLSL)
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

varying vec4 pos;

void main(void)
{
  // https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/7.4.camera_class/7.4.camera.vs
	//gl_Position = projection * view * model * vec4(aPos, 1.0f);
  pos = vec4(vertexpos,1.);
  //pos.x/=2.;
  //pos.y/=2.;
  //pos.z+=.9;
  gl_Position =  projMatrix * viewMatrix * pos;
}
