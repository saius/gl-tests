// g++ zero.cc -lglut -lGL -lGLU -o zero && ./zero

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// GLM, librería utilitaria (OpenGL Mathematics)
//#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4

#include <iostream>
#include <cstdio>
#include "loader.h"
#include "shapes.h"
#include "camera.h"

#define WINDOW_W 600
#define WINDOW_H 600
#define WINDOW_NAME "Camera"

bool* keyStates = new bool[256];
#define CTRL_ON() (keyStates[114] || keyStates[115])

const float moveSensitivity = .04;
const float lookSensitivity = 8.;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

#define VERT_SHADER "vertex.c"
#define FRAG_SHADER "frag.c"
GLuint shaderProgram;
GLuint vertex_array_object;
GLuint vertex_array_len;

// https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/shader.h
void setMat4(const std::string &name, const glm::mat4 &mat)
{
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void draw()
{
  // Set Background Color
  glClearColor(0.5, 0.4, 0.4, 1.0);
  // Clear screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Reset transformations
  glLoadIdentity();

  //glTranslatef(.2, 0, 0);
  //glScalef(.6,.8,.4);
  //drawAxis();

  // https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/7.4.camera_class/camera_class.cpp
  // camera/view transformation
  glm::mat4 view = camera.GetViewMatrix();
  glm::mat4 proj = glm::perspective(glm::radians(camera.Zoom), (float)WINDOW_W / (float)WINDOW_H, 0.1f, 100.0f);

  glUseProgram(shaderProgram);
  setMat4("viewMatrix", view);
  setMat4("projMatrix", proj);
  glBindVertexArray(vertex_array_object);
  glDrawArrays(GL_LINES, 0, vertex_array_len);
  glBindVertexArray(0);
  glUseProgram(0);

  glFlush();
  glutSwapBuffers();
}

void printCoordsInfo(){
  //printf("zoom=%.2f rX=%.2f rY=%.2f\n", zoom, rX, rY);
}

// GLUT_KEY_* special keys
// https://www.opengl.org/resources/libraries/glut/spec3/node54.html
void specialKeyPressed(int key, int x, int y)
{
  printf("specialKeyPressed %i\n", key);
  keyStates[key] = true;
  if (!CTRL_ON()){
    if (key == GLUT_KEY_RIGHT)
    {
        camera.ProcessKeyboard(RIGHT, moveSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_LEFT)
    {
        camera.ProcessKeyboard(LEFT, moveSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_UP)
    {
        camera.ProcessKeyboard(FORWARD, moveSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_DOWN)
    {
        camera.ProcessKeyboard(BACKWARD, moveSensitivity);
        printCoordsInfo();
    }
  }else{
    if (key == GLUT_KEY_RIGHT)
    {
        camera.ProcessMouseMovement(lookSensitivity, 0);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_LEFT)
    {
        camera.ProcessMouseMovement(-lookSensitivity, 0);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_UP)
    {
        camera.ProcessMouseMovement(0, lookSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_DOWN)
    {
        camera.ProcessMouseMovement(0, -lookSensitivity);
        printCoordsInfo();
    }
  }

  // Request display update
  glutPostRedisplay();
}

void specialKeyUp(int key, int x, int y)
{
  printf("specialKeyUp %i\n", key);
  keyStates[key] = false;
}

void keyPressed (unsigned char key, int x, int y) {
  printf("keyPressed %i '%c'\n", key, key);
  switch (key) {
    // ESC
    case 27:
      exit(1);
  }
}

void keyUp (unsigned char key, int x, int y) {
  printf("keyUp %i '%c'\n", key, key);
}

void createShader(){
  float points[] = {
    0,0,0,
    1,0,0,
    0,0,0,
    0,1,0,
    0,0,0,
    0,0,1,
  };
  int pointsN = 6*3;
  vertex_array_len = 6;

  // https://computergraphics.stackexchange.com/questions/10399/drawing-a-square-using-gldrawarrays-with-gl-triangles
  // VBO
  GLuint vertex_buffer_object = 0;
  glGenBuffers(1, &vertex_buffer_object);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pointsN, points, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind

  // VAO
  vertex_array_object = 0;
  glGenVertexArrays(1, &vertex_array_object);
  glBindVertexArray(vertex_array_object);
  glEnableVertexAttribArray(0); // enable the first input variable (vertex position)
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
  // glVertexAttribPointer(index, size, type, normalized, stride, pointer)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
  glBindVertexArray(0); // unbind

  shaderProgram = load_shader(VERT_SHADER, FRAG_SHADER);
}

int main(int argc, char **argv)
{
  // Initialize GLUT and process user parameters
  glutInit(&argc, argv);

  // Request double buffered true color window with Z-buffer
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  glutInitWindowSize(WINDOW_W,WINDOW_H);
  glutInitWindowPosition(100, 100);

  // Create window
  glutCreateWindow(WINDOW_NAME);

  // Enable Z-buffer depth test
  glEnable(GL_DEPTH_TEST);

  // Callback functions
  glutDisplayFunc(draw);
  glutSpecialFunc(specialKeyPressed);
  glutSpecialUpFunc(specialKeyUp);
  glutKeyboardFunc(keyPressed);
  glutKeyboardUpFunc(keyUp);

  createShader();

  // Pass control to GLUT for events
  glutMainLoop();

  return 0;
}
