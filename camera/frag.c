// https://www.khronos.org/opengl/wiki/Fragment_Shader
// gl_FragCoord gl_FrontFacing gl_PrimitiveID
#version 400

varying vec4 pos;

void main (void)
{
  gl_FragColor = vec4(pos.x*2.,pos.y*2.,pos.z*2.,1.);
}
