// g++ zero.cc -lglut -lGL -lGLU -o zero && ./zero

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// GLM, librería utilitaria (OpenGL Mathematics)
//#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966
#define TWO_PI 6.283185307179586

#include <iostream>
#include <cstdio>
#include "loader.h"
#include "shapes.h"
#include "camera.h"

#define WINDOW_W 600
#define WINDOW_H 600
#define WINDOW_NAME "Camera"

// #define = ON | #undef = OFF
#define DEBUG_MOUSE_EVENTS
#undef DEBUG_KEY_EVENTS

bool* keyStates = new bool[256];
#define CTRL_ON() (keyStates[114] || keyStates[115])

const glm::vec3 clearColor = glm::vec3(0.1f, 0.0f, 0.0f);

const float moveSensitivity = .1;
const float lookSensitivity = 8.;
const glm::vec3 initialCamaraPos = glm::vec3(0.0f, 0.0f, 10.0f);

// camera
Camera camera(initialCamaraPos);

#define VERT_SHADER "vertex.c"
#define FRAG_SHADER "frag.c"
GLuint shaderProgram;
GLuint vaoBot, vaoTop, vaoLen;

// https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/shader.h
void setMat4(const std::string &name, const glm::mat4 &mat)
{
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

bool mouseIsDown = false;
int lastMouseT = 0;
glm::vec2 lastMousePos = glm::vec2(0,0);
float mouseSens = .003;
int mouseMaxTimeDiff = 500;
glm::vec2 mouseMove = glm::vec2(0,0);

void mouseClickMove(int x, int y) {
  #ifdef DEBUG_MOUSE_EVENTS
    printf("mouseClickMove %i %i\n", x, y);
  #endif

  int elapsedTime = glutGet(GLUT_ELAPSED_TIME);

  // first mouse down
  if (!mouseIsDown){
    #ifdef DEBUG_MOUSE_EVENTS
      printf("mouseDown\n");
    #endif
    lastMousePos.x = x;
    lastMousePos.y = y;
  }else{
    // para movimiento normal del mouse va de 8 a 64 este valor
    int timeDiff = elapsedTime - lastMouseT;
    if (timeDiff == 0) return;
    if (timeDiff > mouseMaxTimeDiff) {
      #ifdef DEBUG_MOUSE_EVENTS
        printf("mouseClickMove timeout\n");
      #endif
      lastMousePos.x = x;
      lastMousePos.y = y;
      lastMouseT = elapsedTime;
      return;
    }
    glm::vec2 mousePos = glm::vec2(x,y);
    glm::vec2 mouseDiff = lastMousePos - mousePos;
    mouseMove = mouseDiff * mouseSens * (float)timeDiff;
  }
  lastMouseT = elapsedTime;

  mouseIsDown = true;

  // Request display update
  glutPostRedisplay();
}

void mousePassiveMove(int x, int y) {
  #ifdef DEBUG_MOUSE_EVENTS
    //printf("mousePassiveMove %i %i\n", x, y);
  #endif
  if (mouseIsDown){
    #ifdef DEBUG_MOUSE_EVENTS
      printf("mouseUp (late)\n");
    #endif
    mouseIsDown = false;

    // Request display update
    glutPostRedisplay();
  }
}

void drawTriag(glm::mat4 proj, glm::vec3 pos){
  glm::mat4 modifiedProj = glm::translate(proj, pos);
  setMat4("projMatrix", modifiedProj);

  // https://docs.gl/gl3/glDrawArrays
  glBindVertexArray(vaoBot);
  glDrawArrays(GL_TRIANGLE_FAN, 0, vaoLen);
  glBindVertexArray(vaoTop);
  glDrawArrays(GL_TRIANGLE_FAN, 0, vaoLen);
}

void draw()
{
  // Set Background Color
  glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0);
  // Clear screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Reset transformations
  glLoadIdentity();

  //glTranslatef(.2, 0, 0);
  //glScalef(.6,.8,.4);
  //drawAxis();

  // https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/7.4.camera_class/camera_class.cpp
  // camera/view transformation
  if (mouseMove.x != 0 && mouseMove.y != 0){
    printf("camera mouseMove %.2f %.2f\n", mouseMove.x, mouseMove.y);
    camera.ProcessMouseMovement(-mouseMove.x, mouseMove.y);
    mouseMove.x = mouseMove.y = 0;
  }
  glm::mat4 view = camera.GetViewMatrix();
  glm::mat4 proj = glm::perspective(
    glm::radians(camera.Zoom),
    (float)WINDOW_W / (float)WINDOW_H,
    0.1f,
    100.0f
  );


  glUseProgram(shaderProgram);
  setMat4("viewMatrix", view);
  setMat4("projMatrix", proj);

  drawAxis();

  /*int trigCount = 3;
  float trigSep = 2.;
  float xOffset = -((trigCount-1)/2.) * trigSep;
  for (int i = 0; i < trigCount; i++) {
    float x = xOffset + i*trigSep;
    drawTriag(proj, glm::vec3(x, 0, 0));
  }*/

  drawTriag(proj, glm::vec3(1, 0, -6));
  drawTriag(proj, glm::vec3(-1, 3, -4));
  drawTriag(proj, glm::vec3(2, -1, -2));
  drawTriag(proj, glm::vec3(1, 2, 1));
  drawTriag(proj, glm::vec3(1, -2, 2));
  drawTriag(proj, glm::vec3(-2, -1, 2));
  drawTriag(proj, glm::vec3(3, -1, 4));

  glBindVertexArray(0);

  glUseProgram(0);

  glFlush();

  glutSwapBuffers();
}

void printCoordsInfo(){
  //printf("zoom=%.2f rX=%.2f rY=%.2f\n", zoom, rX, rY);
}

// GLUT_KEY_* special keys
// https://www.opengl.org/resources/libraries/glut/spec3/node54.html
void specialKeyPressed(int key, int x, int y)
{
  #ifdef DEBUG_KEY_EVENTS
    printf("specialKeyPressed %i\n", key);
  #endif
  keyStates[key] = true;
  if (!CTRL_ON()){
    if (key == GLUT_KEY_RIGHT)
    {
        camera.ProcessKeyboard(RIGHT, moveSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_LEFT)
    {
        camera.ProcessKeyboard(LEFT, moveSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_UP)
    {
        camera.ProcessKeyboard(FORWARD, moveSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_DOWN)
    {
        camera.ProcessKeyboard(BACKWARD, moveSensitivity);
        printCoordsInfo();
    }
  }else{
    if (key == GLUT_KEY_RIGHT)
    {
        camera.ProcessMouseMovement(lookSensitivity, 0);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_LEFT)
    {
        camera.ProcessMouseMovement(-lookSensitivity, 0);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_UP)
    {
        camera.ProcessMouseMovement(0, lookSensitivity);
        printCoordsInfo();
    }
    else if (key == GLUT_KEY_DOWN)
    {
        camera.ProcessMouseMovement(0, -lookSensitivity);
        printCoordsInfo();
    }
  }

  // Request display update
  glutPostRedisplay();
}

void specialKeyUp(int key, int x, int y)
{
  #ifdef DEBUG_KEY_EVENTS
    printf("specialKeyUp %i\n", key);
  #endif
  keyStates[key] = false;
}

void keyPressed (unsigned char key, int x, int y) {
  #ifdef DEBUG_KEY_EVENTS
    printf("keyPressed %i '%c'\n", key, key);
  #endif
  switch (key) {
    // ESC
    case 27:
      exit(1);
  }
}

void keyUp (unsigned char key, int x, int y) {
  #ifdef DEBUG_KEY_EVENTS
    printf("keyUp %i '%c'\n", key, key);
  #endif
}

GLuint createVBOf(int vertsLen, float* verts){
  GLuint vbo;
  // https://docs.gl/gl3/glGenBuffers
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertsLen, verts, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
  return vbo;
}

GLuint createVAO3f(GLuint vbo){
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vbo);
  glEnableVertexAttribArray(0); // enable the first input variable (vertex position)
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  // glVertexAttribPointer(index, size, type, normalized, stride, pointer)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
  glBindVertexArray(0); // unbind
  return vao;
}

void createShader(){
  shaderProgram = load_shader(VERT_SHADER, FRAG_SHADER);

  // VERTS
  float vertsBot[] = {
    0,-1,0,
    glm::cos(0),0,glm::sin(0),
    glm::cos(HALF_PI),0,glm::sin(HALF_PI),
    glm::cos(PI),0,glm::sin(PI),
    glm::cos(PI+HALF_PI),0,glm::sin(PI+HALF_PI),
    glm::cos(TWO_PI),0,glm::sin(TWO_PI),
    glm::cos(0),0,glm::sin(0),
  };
  float vertsTop[] = {
    0,1,0,
    glm::cos(0),0,glm::sin(0),
    glm::cos(HALF_PI),0,glm::sin(HALF_PI),
    glm::cos(PI),0,glm::sin(PI),
    glm::cos(PI+HALF_PI),0,glm::sin(PI+HALF_PI),
    glm::cos(TWO_PI),0,glm::sin(TWO_PI),
    glm::cos(0),0,glm::sin(0),
  };
  int vertsCount = 7*3;
  vaoLen = 7;

  // https://computergraphics.stackexchange.com/questions/10399/drawing-a-square-using-gldrawarrays-with-gl-triangles
  // VBOs
  GLuint vboBot = 0, vboTop = 0;
  vboBot = createVBOf(vertsCount, vertsBot);
  vboTop = createVBOf(vertsCount, vertsTop);
  // VAO
  vaoBot = createVAO3f(vboBot);
  vaoTop = createVAO3f(vboTop);
}

int main(int argc, char **argv)
{
  // Initialize GLUT and process user parameters
  glutInit(&argc, argv);

  // Request double buffered true color window with Z-buffer
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  glutInitWindowSize(WINDOW_W,WINDOW_H);
  glutInitWindowPosition(100, 100);

  // Create window
  glutCreateWindow(WINDOW_NAME);

  // Enable Z-buffer depth test
  glEnable(GL_DEPTH_TEST);

  // Callback functions
  glutDisplayFunc(draw);
  glutSpecialFunc(specialKeyPressed);
  glutSpecialUpFunc(specialKeyUp);
  glutKeyboardFunc(keyPressed);
  glutKeyboardUpFunc(keyUp);
  glutMotionFunc(mouseClickMove);
  glutPassiveMotionFunc(mousePassiveMove);

  createShader();

  // Pass control to GLUT for events
  glutMainLoop();

  return 0;
}
