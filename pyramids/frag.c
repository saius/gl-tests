// https://www.khronos.org/opengl/wiki/Fragment_Shader
// gl_FragCoord gl_FrontFacing gl_PrimitiveID
#version 400

varying vec4 pos;

void main (void)
{
  //float d = distance(vec3(0,0,0), pos.xyz);
  float colX = (pos.x+1)/2.;
  float colY = (pos.y+1)/2.;
  float colZ = (pos.z+1)/2.;
  gl_FragColor = vec4(colX,colY,colZ,0);
}
