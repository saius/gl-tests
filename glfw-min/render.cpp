
// glad importa las librerías base de opengl
#include <glad/gl.h>
#include <GLFW/glfw3.h>

// GLM, librería math utilitaria (OpenGL Mathematics)
//#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/trigonometric.hpp> // glm::sin, etc

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966
#define TWO_PI 6.283185307179586

#define _log(MSG) std::cout << MSG << std::endl;
#define logf printf

// para std::cout/endl
#include <iostream>
// para std::rand
#include <cstdlib>
#include <vector>

#include "render.h"
#include "camera.h"
#include "shaders/shader.h"

#define VERT_SHADER "shaders/vert.c"
#define FRAG_SHADER "shaders/frag.c"

const int cubesN = 60;
const float cubesTMin = 0.;
const float cubesTVar = 14.;
const float cubesSMin = .1;
const float cubesSVar = 1.8;
// costado
//const glm::vec3 initialCamaraPos = glm::vec3(1.0f, .5f, 3.0f);
//float rotateXSpeed=-.2, rotateYSpeed=-.1;
// centro
const glm::vec3 initialCamaraPos = glm::vec3(0.,0.,0.);
float rotateXSpeed=7., rotateYSpeed=5.;

Camera camera(initialCamaraPos);

Renderer::Renderer () {
  shader.createShader(VERT_SHADER, FRAG_SHADER);
  loadAxis();
  loadCube();
  createCubes();
}

void Renderer::loadAxis(){
  float verts[] = {
    0,0,0,
    1,0,0,
    0,0,0,
    0,1,0,
    0,0,0,
    0,0,1,
  };
  int vertsLen = 6;
  int vertsCount = vertsLen*3;

  // https://computergraphics.stackexchange.com/questions/10399/drawing-a-square-using-gldrawarrays-with-gl-triangles
  // VBOs
  GLuint vboID = 0, vaoID = 0;
  vboID = shader.createVBOf(vertsCount, verts);
  // VAO
  vaoID = shader.createVAO3f(vboID);

  // public vars
  axisArrayID = vaoID;
  axisArrayLen = vertsCount;
}

void Renderer::loadCube(){
  float verts[] = {
    // Top face
    1.0f, 1.0f, -1.0f ,
    -1.0f, 1.0f, -1.0f ,
    -1.0f, 1.0f,  1.0f ,
    1.0f, 1.0f,  1.0f ,
    // Bottom face
    1.0f, -1.0f, -1.0f ,
    -1.0f, -1.0f, -1.0f ,
    -1.0f, -1.0f,  1.0f ,
    1.0f, -1.0f,  1.0f ,
    // Front face
    1.0f,  1.0f, 1.0f ,
    -1.0f,  1.0f, 1.0f ,
    -1.0f, -1.0f, 1.0f ,
    1.0f, -1.0f, 1.0f ,
    // Back face
    1.0f, -1.0f, -1.0f ,
    -1.0f, -1.0f, -1.0f ,
    -1.0f,  1.0f, -1.0f ,
    1.0f,  1.0f, -1.0f ,
    // Left face
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    // Right face
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f,  1.0f,
  };
  int vertsLen = 4*6;
  int vertsCount = vertsLen*3;

  // https://computergraphics.stackexchange.com/questions/10399/drawing-a-square-using-gldrawarrays-with-gl-triangles
  // VBOs
  GLuint vboID = 0, vaoID = 0;
  vboID = shader.createVBOf(vertsCount, verts);
  // VAO
  vaoID = shader.createVAO3f(vboID);

  // public vars
  cubeArrayID = vaoID;
  cubeArrayLen = vertsCount;
}

float randScalef(){
  // https://www.cplusplus.com/reference/cstdlib/rand/
  float normalized = (rand()%1000)/1000.;
  return cubesSMin + normalized*cubesSVar;
}
float randTranslatef(){
  float min = cubesTMin;
  float var = cubesTVar;
  float normalized = ((rand()%2000)-1000)/1000.;
  // TODO cambiar a polar, no cuadrado!
  if (normalized >= 0) return min + normalized*var;
  else return -min + normalized*var;
}
glm::vec3 randScaleV3(){
  return glm::vec3(randScalef(),randScalef(),randScalef());
}
glm::vec3 randTranslateV3(){
  // TODO random polar no cuadrado!
  return glm::vec3(randTranslatef(),randTranslatef(),randTranslatef());
}
void Renderer::createCubes(){
  int transformsLen = cubesN;

  // https://www.programiz.com/cpp-programming/vectors
  std::vector<glm::vec3> scaleTransforms;
  std::vector<glm::vec3> translateTransforms;

  // https://www.cplusplus.com/reference/cstdlib/rand/
  /* initialize random seed: */
  srand(time(NULL));
  for (int i = 0; i < transformsLen; i++) {
    glm::vec3 randTranslate = randTranslateV3();
    scaleTransforms.push_back(randScaleV3());
    translateTransforms.push_back(randTranslate);
    //logf("T#%i %.2f %.2f %.2f\n",i,randTranslate.x,randTranslate.y,randTranslate.z);
  }

  // public vars
  cubes = transformsLen;
  cubesScaleTransforms = scaleTransforms;
  cubesTranslateTransforms = translateTransforms;
}

void Renderer::drawAxis(){
  // https://docs.gl/gl3/glDrawArrays
  glBindVertexArray(axisArrayID);
  glDrawArrays(GL_LINES, 0, axisArrayLen);
  glBindVertexArray(0);
}

void Renderer::drawCube(){
  // https://docs.gl/gl3/glDrawArrays
  glBindVertexArray(cubeArrayID);
  glDrawArrays(GL_QUADS, 0, cubeArrayLen);
  glBindVertexArray(0);
}

void Renderer::draw()
{
  double time = glfwGetTime();
  //logf("Render time %f\n", time);

  float colRed = .5 + glm::sin(TWO_PI*(time/4.)) / 2.;

  // Set Background Color
  //glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0);
  glClearColor(colRed, clearColor.g, clearColor.b, 1.0);
  // Clear screen
  //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClear(GL_COLOR_BUFFER_BIT);

  camera.ProcessMouseMovement(rotateXSpeed, rotateYSpeed);
  if (camera.Pitch >= 89.0f && rotateYSpeed > 0)
      rotateYSpeed = -rotateYSpeed;
  if (camera.Pitch <= -89.0f && rotateYSpeed < 0)
      rotateYSpeed = -rotateYSpeed;

  // Model matrix : an identity matrix (model will be at the origin)
  glm::mat4 identity = glm::mat4(1.0f);
  glm::mat4 model = identity;
  glm::mat4 view = camera.GetViewMatrix();
  glm::mat4 projection = glm::perspective(
    glm::radians(camera.Zoom),
    (float)windowWidth / (float)windowHeight,
    0.1f,
    100.0f
  );

  // Our ModelViewProjection : multiplication of our 3 matrices
  glm::mat4 mvp = projection * view * model; // Remember, matrix multiplication is the other way around

  glUseProgram(shader.getShaderProgram());

    shader.setMat4("MVP", mvp);
    drawAxis();

    for (int i = 0; i < cubes; i++) {
      model = glm::scale(identity, cubesScaleTransforms.at(i));
      model = glm::translate(model, cubesTranslateTransforms.at(i));
      mvp = projection * view * model;
      shader.setMat4("MVP", mvp);
      drawCube();
    }

  glUseProgram(0);

  // ??
  //glFlush();
}
