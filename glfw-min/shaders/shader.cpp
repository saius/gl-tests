// GLM, librería utilitaria (OpenGL Mathematics)
//#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4

// para std::string
#include <iostream>

#include "shader.h"
#include "loader.h"

// https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/shader.h
void Shader::setMat4(const std::string &name, const glm::mat4 &mat)
{
  // Get a handle for our "MVP" uniform
  // Only during the initialisation
  //GLuint uniformId = glGetUniformLocation(shaderProgram, "MVP");
  glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

GLuint Shader::createVBOf(int vertsLen, float* verts){
  GLuint vbo;
  // https://docs.gl/gl3/glGenBuffers
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertsLen, verts, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
  return vbo;
}

GLuint Shader::createVAO3f(GLuint vbo){
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vbo);
  glEnableVertexAttribArray(0); // enable the first input variable (vertex position)
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  // glVertexAttribPointer(index, size, type, normalized, stride, pointer)
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
  glBindVertexArray(0); // unbind
  return vao;
}

void Shader::createShader(const char* vertexName, const char* fragName){
  shaderProgram = load_shader(vertexName, fragName);
}
