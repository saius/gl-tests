// https://www.khronos.org/opengl/wiki/Vertex_Shader
#version 400

layout(location = 0) in vec3 vertexPos;

// https://www.khronos.org/opengl/wiki/Uniform_(GLSL)
uniform mat4 MVP;

varying vec4 pos;

void main(void)
{
  // https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/7.4.camera_class/7.4.camera.vs
	//gl_Position = projection * view * model * vec4(aPos, 1.0f);
  pos = vec4(vertexPos, 1.);
  gl_Position =  MVP * pos;
}
