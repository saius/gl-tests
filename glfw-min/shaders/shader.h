#ifndef  __SHADER_H__
#define  __SHADER_H__

#include <glad/gl.h>
#include <glm/mat4x4.hpp>

class Shader {
    GLuint shaderProgram;
  public:
    GLuint getShaderProgram(){return shaderProgram;};
    void setMat4(const std::string &name, const glm::mat4 &mat);
    GLuint createVBOf(int vertsLen, float* verts);
    GLuint createVAO3f(GLuint vbo);
    void createShader(const char* vertexName, const char* fragName);
};

#endif /* __SHADER_H__ */
