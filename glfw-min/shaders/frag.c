// https://www.khronos.org/opengl/wiki/Fragment_Shader
// gl_FragCoord gl_FrontFacing gl_PrimitiveID
#version 400

varying vec4 pos;

void main (void)
{
  float colX = pos.x;
  float colY = pos.y;
  float colZ = pos.z;
  gl_FragColor = vec4(colX,colY,colZ,0);
}
