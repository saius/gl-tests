#include <vector>

#include "shaders/shader.h"

class Renderer {
  public:
    Shader shader;
    glm::vec3 clearColor;
    float windowWidth;
    float windowHeight;
    GLuint axisArrayID;
    GLuint cubeArrayID;
    int axisArrayLen;
    int cubeArrayLen;
    int cubes;
    std::vector<glm::vec3> cubesScaleTransforms;
    std::vector<glm::vec3> cubesTranslateTransforms;
    void loadAxis();
    void loadCube();
    void createCubes();

    Renderer();
    void drawAxis();
    void drawCube();
    void draw();
};
