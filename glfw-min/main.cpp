// instalar paquete de glfw (glfw-x11 en arch)
// make && ./main

// ??
//#define GL_GLEXT_PROTOTYPES

// glad importa las librerías base de opengl
#include <glad/gl.h>
#include <GLFW/glfw3.h>

// GLM, librería math utilitaria (OpenGL Mathematics)
//#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/trigonometric.hpp> // glm::sin, etc

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966
#define TWO_PI 6.283185307179586

#define _log(MSG) std::cout << MSG << std::endl;
#define logf printf

// para std::cout/endl
#include <iostream>

#include "render.h"

#define OPENGL_VERSION_MAJOR 3
#define OPENGL_VERSION_MINOR 1
#define OPENGL_CORE false
#define WINDOW_W 600
#define WINDOW_H 600
#define WINDOW_TITLE "GLFW Test!"

const glm::vec3 clearColor = glm::vec3(0.1f, 0.2f, 0.2f);

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

int main(int argc, char **argv){
  _log("Comenzando programa");

  // código traído de:
  // https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/1.2.hello_window_clear/hello_window_clear.cpp
  if (!glfwInit()){
      _log("Failed to init GLFW library");
      return -1;
  }

  // version de opengl mínima requerida
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OPENGL_VERSION_MAJOR);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OPENGL_VERSION_MINOR);

  // activar según cómo se haya generado glad
  if (OPENGL_CORE){
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  }

  #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  #endif

  GLFWwindow* window = glfwCreateWindow(WINDOW_W, WINDOW_H, WINDOW_TITLE, NULL, NULL);
  if (window == NULL)
  {
      _log("Failed to create GLFW window");
      glfwTerminate();
      return -1;
  }
  glfwMakeContextCurrent(window);
  _log("Ventana creada (GLFW)");

  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

  // glad: load all OpenGL function pointers
  // ---------------------------------------
  //if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
  if (!gladLoaderLoadGL())
  {
      _log("Failed to initialize GLAD");
      return -1;
  }

  Renderer renderer;
  renderer.windowWidth = WINDOW_W;
  renderer.windowHeight = WINDOW_H;
  renderer.clearColor = clearColor;

  // render loop
  // -----------
  while (!glfwWindowShouldClose(window))
  {
      // input
      // -----
      processInput(window);

      // render
      // ------
      renderer.draw();

      // Swap front and back buffers
      glfwSwapBuffers(window);

      // Poll and process IO events (keys pressed/released, mouse moved etc.)
      glfwPollEvents();
  }

  _log("Terminando programa");
  glfwTerminate();
  return 0;
}

// https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/1.2.hello_window_clear/hello_window_clear.cpp
// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    logf("Window resize %i %i\n", width, height);
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
