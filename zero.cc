// g++ zero.cc -lglut -lGL -lGLU -o zero && ./zero

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iostream>
#include <cstdio>

#define WINDOW_W 600
#define WINDOW_H 600
#define WINDOW_NAME "Zero win"

bool* keyStates = new bool[256];
#define CTRL_ON() (keyStates[114] || keyStates[115])

// Rotate X
double rX=0;
// Rotate Y
double rY=0;
double rSpeed=10;

double zoom=1.5;
float zoomStep=0.07;

void draw()
{
  // Set Background Color
  glClearColor(0.8, 0.4, 0.4, 1.0);
  // Clear screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Reset transformations
  glLoadIdentity();

  glRotatef( rX, 0.0, 1.0, 0.0 );
  glRotatef( rY, 1.0, 0.0, 0.0 );
  //glTranslatef(.2, 0, 0);
  //glScalef(.6,.8,.4);

  glFlush();
  glutSwapBuffers();
}

void printCoordsInfo(){
  printf("zoom=%.2f rX=%.2f rY=%.2f\n", zoom, rX, rY);
}

// GLUT_KEY_* special keys
// https://www.opengl.org/resources/libraries/glut/spec3/node54.html
void specialKeyPressed(int key, int x, int y)
{
  printf("specialKeyPressed %i\n", key);
  keyStates[key] = true;
  if (key == GLUT_KEY_RIGHT)
  {
      rX += rSpeed;
      printCoordsInfo();
  }
  else if (key == GLUT_KEY_LEFT)
  {
      rX -= rSpeed;
      printCoordsInfo();
  }
  else if (!CTRL_ON() && key == GLUT_KEY_DOWN)
  {
      rY -= rSpeed;
      printCoordsInfo();
  }
  else if (!CTRL_ON() && key == GLUT_KEY_UP)
  {
      rY += rSpeed;
      printCoordsInfo();
  }
  else if (CTRL_ON() && key == GLUT_KEY_DOWN)
  {
      zoom -= zoomStep;
      printCoordsInfo();
  }
  else if (CTRL_ON() && key == GLUT_KEY_UP)
  {
      zoom += zoomStep;
      printCoordsInfo();
  }

  // Request display update
  glutPostRedisplay();
}

void specialKeyUp(int key, int x, int y)
{
  printf("specialKeyUp %i\n", key);
  keyStates[key] = false;
}

void keyPressed (unsigned char key, int x, int y) {
  printf("keyPressed %i '%c'\n", key, key);
  switch (key) {
    // ESC
    case 27:
      exit(1);
  }
}

void keyUp (unsigned char key, int x, int y) {
  printf("keyUp %i '%c'\n", key, key);
}

int main(int argc, char **argv)
{
  // Initialize GLUT and process user parameters
  glutInit(&argc, argv);

  // Request double buffered true color window with Z-buffer
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  glutInitWindowSize(WINDOW_W,WINDOW_H);
  glutInitWindowPosition(100, 100);

  // Create window
  glutCreateWindow(WINDOW_NAME);

  // Enable Z-buffer depth test
  glEnable(GL_DEPTH_TEST);

  // Callback functions
  glutDisplayFunc(draw);
  glutSpecialFunc(specialKeyPressed);
  glutSpecialUpFunc(specialKeyUp);
  glutKeyboardFunc(keyPressed);
  glutKeyboardUpFunc(keyUp);

  // Pass control to GLUT for events
  glutMainLoop();

  return 0;
}
