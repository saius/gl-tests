
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

// https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/shader.h
// constructor generates the shader on the fly
// ------------------------------------------------------------------------
unsigned int load_shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr)
{
    unsigned int ID;
    // 1. retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    std::ifstream gShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    gShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        // open files
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
        // if geometry shader path is present, also load a geometry shader
        if(geometryPath != nullptr)
        {
            gShaderFile.open(geometryPath);
            std::stringstream gShaderStream;
            gShaderStream << gShaderFile.rdbuf();
            gShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch (std::ifstream::failure& e)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
    const char* vShaderCode = vertexCode.c_str();
    const char * fShaderCode = fragmentCode.c_str();
    // 2. compile shaders
    unsigned int vertex, fragment;
    // vertex shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    GLint test;
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &test);
    if(!test) {
    	std::cerr << "Shader[vertex] compilation failed with this message:" << std::endl;
    	std::vector<char> compilation_log(512);
    	glGetShaderInfoLog(vertex, compilation_log.size(), NULL, &compilation_log[0]);
    	std::cerr << &compilation_log[0] << std::endl;
    	//glfwTerminate();
      //glutLeaveMainLoop();
    	exit(-1);
    }
  	std::cout << "Shader[vertex] compilation OK" << std::endl;
    //checkCompileErrors(vertex, "VERTEX");
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    //GLint test;
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &test);
    if(!test) {
    	std::cerr << "Shader[fragment] compilation failed with this message:" << std::endl;
    	std::vector<char> compilation_log(512);
    	glGetShaderInfoLog(fragment, compilation_log.size(), NULL, &compilation_log[0]);
    	std::cerr << &compilation_log[0] << std::endl;
    	//glfwTerminate();
      //glutLeaveMainLoop();
    	exit(-1);
    }
  	std::cout << "Shader[fragment] compilation OK" << std::endl;
    //checkCompileErrors(fragment, "FRAGMENT");
    // if geometry shader is given, compile geometry shader
    unsigned int geometry;
    if(geometryPath != nullptr)
    {
        const char * gShaderCode = geometryCode.c_str();
        geometry = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geometry, 1, &gShaderCode, NULL);
        glCompileShader(geometry);
        //checkCompileErrors(geometry, "GEOMETRY");
    }

    // shader Program
    ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    if(geometryPath != nullptr)
        glAttachShader(ID, geometry);

        //glBindAttribLocation(ID, 0, "time");
        //glEnableVertexAttribArray(0);

    glLinkProgram(ID);
    //checkCompileErrors(ID, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    if(geometryPath != nullptr)
        glDeleteShader(geometry);

    return ID;
}
