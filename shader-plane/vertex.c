// https://www.khronos.org/opengl/wiki/Vertex_Shader
//#version 400
#version 110

//layout(location = 0) in vec3 vertexpos;
attribute vec3 vertexpos;

// https://www.khronos.org/opengl/wiki/Uniform_(GLSL)
//uniform mat4 MVP;
//uniform mat4 proy;
uniform int time;
//uniform float height;

varying vec4 pos;

#define PI 3.1415926535897932384626433832795
#define TWO_PI 6.283185307179586

const float loopSep = 1000.;
const float ySpeed=.0001;

// returns from (0,1)
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void)
{
  //pos = vec4(vertexpos,1.);
  //pos.z+=2.*sin(pos.x+pos.y);
  //gl_Position = MVP * proy * pos;
  //pos = gl_Vertex;
  pos = vec4(vertexpos,1.);
  //float varT = sin(TWO_PI*time/1000.);
  //pos.z += varT*2.;

  /*// animate!
  float loopTime = mod(time,loopSep*4.);
  vec2 loopSeed;
  if (loopTime < loopSep*1.) loopSeed=vec2(pos.x,pos.y);
  else if (loopTime < loopSep*2.) loopSeed=vec2(-pos.x,-pos.y);
  else if (loopTime < loopSep*3.) loopSeed=vec2(-pos.x,pos.y);
  else loopSeed=vec2(pos.x,-pos.y);
  pos.z += rand(loopSeed)*height;//*/

  // random peaks
  pos.z = -.5 + rand(vec2(pos.x,pos.y));

  // move forward
  float yMove = float(time)*ySpeed;
  // normalize
  pos.y = fract(pos.y+yMove);

  gl_Position = gl_ModelViewProjectionMatrix * pos;
}
