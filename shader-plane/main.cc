// g++ main.cc shapes.cc -lglut -lGL -lGLU -o main && ./main
// g++ *.cc -lglut -lGL -lGLU -o main && ./main

#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// GLM, librería utilitaria (OpenGL Mathematics)
//#include <glm/gtx/io.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4

#include <iostream>
#include <cstdio>
#include "shapes.h"
#include "loader.h"

#define WINDOW_W 700
#define WINDOW_H 700
#define WINDOW_NAME "Shader plane"

// #define on / #undef off
#define DO_SHADER
#define VERT_SHADER "vertex.c"
#define FRAG_SHADER "frag.c"

#define V3(v) v,v,v

bool* keyStates = new bool[256];
#define CTRL_ON() (keyStates[114] || keyStates[115])

#ifdef DO_SHADER
  GLuint shaderProgram;
  GLuint vertex_array_object;
#endif

// Rotate X
double rX=0;
// Rotate Yg
double rY=0;
double rSpeed=1;

double zoom=0.1;
float zoomStep=0.07;

float height=.2;
int meshX = 30, meshY = 30;

//zoom=3.04;rX=0.;rY=-150.;
//#define CAM_POSITION() zoom=1.13;rX=0.;rY=230.;
#define CAM_POSITION() zoom=2.53;rX=0.;rY=238.;

bool showAxis = false;
bool logElapsedTime = false;

void draw()
{
  //glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);

  // Set Background Color
  // orange
  //glClearColor(0.8, 0.4, 0.1, 1.0);
  // black
  glClearColor(0,0,0, 1.0);
  // Clear screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Reset transformations
  glLoadIdentity();

  glRotatef( rX, 0.0, 1.0, 0.0 );
  glRotatef( rY, 1.0, 0.0, 0.0 );
  glScalef(zoom,zoom,zoom);

  if (showAxis){
    glPushMatrix();
      //glScalef(V3(2));
      drawAxis();
    glPopMatrix();
  }

  // se rompe con vertex shader actual
  /*glPushMatrix();
    glTranslatef(.5, 0, 0);
    glScalef(V3(0.05));
    drawPlane(10,10);
  glPopMatrix();*/

  // inputs para vertex shader v400
  /*GLfloat mvp[16];
  GLfloat proy[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, mvp);
  glGetFloatv(GL_PROJECTION_MATRIX, proy);
  GLint uniMvp = glGetUniformLocation(shaderProgram, "MVP");
  glUniformMatrix4fv(uniMvp, 1, GL_FALSE, mvp);
  GLint uniProy = glGetUniformLocation(shaderProgram, "proy");
  glUniformMatrix4fv(uniProy, 1, GL_FALSE, proy);*/

  int elapsedTime = glutGet(GLUT_ELAPSED_TIME);
  if (logElapsedTime) std::cout << elapsedTime << std::endl;
  glPushMatrix();
    // center
    glTranslatef(-.5, -.5, 0);
    //float yMove = .0001*elapsedTime;
    //glTranslatef(0, yMove, 0);
    //glScalef(V3(0.2));
    glScalef(1,1,height);
    //glScalef(1,1.2,1);
    // https://computergraphics.stackexchange.com/questions/10399/drawing-a-square-using-gldrawarrays-with-gl-triangles
    glUseProgram(shaderProgram);
      glUniform1i(glGetUniformLocation(shaderProgram, "time"), elapsedTime);
      glBindVertexArray(vertex_array_object);
      float ySpeed=.0001;
      float yMove = elapsedTime*ySpeed;
      //pos.y = glm::fract(pos.y+yMove);
      for (int j = 0; j < meshY; j++) {
      //for (int j = meshY-1; j >= 0; j--) {
      //for (int j = 2; j < meshY-2; j++) {
        // https://docs.gl/gl3/glDrawArrays
        int k = meshY-(int)glm::floor(elapsedTime*.003)%meshY;
        if (j==k-1) continue;
        if (j==k) continue;
        if (j==k+1) continue;
        if (j==k+2) continue;
        glDrawArrays(GL_TRIANGLE_STRIP, j * meshX * 2, meshX * 2);
      }
      //glDrawArrays(GL_TRIANGLES, 0, meshX * meshY * 2);
      glBindVertexArray(0);
    glUseProgram(0);
  glPopMatrix();

  glFlush();
  glutSwapBuffers();

  // ??
  glutPostRedisplay();
}

void printCoordsInfo(){
  printf("zoom=%.2f rX=%.2f rY=%.2f\n", zoom, rX, rY);
}

void specialKeyPressed(int key, int x, int y)
{
  printf("specialKeyPressed %i\n", key);
  keyStates[key] = true;
  if (key == GLUT_KEY_RIGHT)
  {
      rX += rSpeed;
      printCoordsInfo();
  }
  else if (key == GLUT_KEY_LEFT)
  {
      rX -= rSpeed;
      printCoordsInfo();
  }
  else if (!CTRL_ON() && key == GLUT_KEY_DOWN)
  {
      rY -= rSpeed;
      printCoordsInfo();
  }
  else if (!CTRL_ON() && key == GLUT_KEY_UP)
  {
      rY += rSpeed;
      printCoordsInfo();
  }
  else if (CTRL_ON() && key == GLUT_KEY_DOWN)
  {
      zoom -= zoomStep;
      printCoordsInfo();
  }
  else if (CTRL_ON() && key == GLUT_KEY_UP)
  {
      zoom += zoomStep;
      printCoordsInfo();
  }

  // Request display update
  glutPostRedisplay();
}

void specialKeyUp(int key, int x, int y)
{
  printf("specialKeyUp %i\n", key);
  keyStates[key] = false;
}

void keyPressed (unsigned char key, int x, int y) {
  printf("keyPressed %i '%c'\n", key, key);
  switch (key) {
    // ESC
    case 27:
      exit(1);
    case 'q':
      height-=.01;
      break;
    case 'w':
      height+=.01;
      break;
  }
}

void keyUp (unsigned char key, int x, int y) {
  printf("keyUp %i '%c'\n", key, key);
}


int main(int argc, char **argv)
{
  CAM_POSITION()

  // Initialize GLUT and process user parameters
  glutInit(&argc, argv);

  // Request double buffered true color window with Z-buffer
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  glutInitWindowSize(WINDOW_W,WINDOW_H);
  glutInitWindowPosition(100, 100);

  // Create window
  glutCreateWindow(WINDOW_NAME);

  // Enable Z-buffer depth test
  glEnable(GL_DEPTH_TEST);

  // Callback functions
  glutDisplayFunc(draw);
  glutSpecialFunc(specialKeyPressed);
  glutSpecialUpFunc(specialKeyUp);
  glutKeyboardFunc(keyPressed);
  glutKeyboardUpFunc(keyUp);

  #ifdef DO_SHADER
    shaderProgram = load_shader(VERT_SHADER, FRAG_SHADER);
    glUseProgram(shaderProgram);

    int uniformLocation = glGetUniformLocation(shaderProgram, "height");
    glUniform1f(uniformLocation, height);

    //float *planePoints = getPlanePoints(8,8);
    float *planePoints = new float[meshX * meshY * 3 * 2];
    getPlanePoints(meshX, meshY, planePoints);
    /*for (size_t i = 0; i < 10; i++) {
      std::cout << i << ": " << planePoints[i] << std::endl;
    }//*/

    glBindAttribLocation(shaderProgram, 0, "vertexpos");
    // define an array of generic vertex attribute data
    //glEnableVertexAttribArray(0);
    //glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, planePoints);//*/

    // https://computergraphics.stackexchange.com/questions/10399/drawing-a-square-using-gldrawarrays-with-gl-triangles
    GLuint vertex_buffer_object = 0; // VBO
    glGenBuffers(1, &vertex_buffer_object);
    {
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);

        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshX * meshY * 3 * 2, planePoints, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind
    }

    vertex_array_object = 0; // VAO
    glGenVertexArrays(1, &vertex_array_object);
    {
        glBindVertexArray(vertex_array_object);

        glEnableVertexAttribArray(0); // enable the first input variable (vertex position)
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
        // glVertexAttribPointer(index, size, type, normalized, stride, pointer)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    // don't leave enabled by default
    glUseProgram(0);

    // https://stackoverflow.com/questions/19102180/how-does-gldrawarrays-know-what-to-draw
    /*glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject); //Bind GL_ARRAY_BUFFER to our handle
    glEnableVertexAttribArray(0); //?
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0); //Information about the array, 3 points for each vertex, using the float type, don't normalize, no stepping, and an offset of 0. I don't know what the first parameter does however, and how does this function know which array to deal with (does it always assume we're talking about GL_ARRAY_BUFFER?

    glDrawArrays(GL_POINTS, 0, 1); //Draw the vertices, once again how does this know which vertices to draw? (Does it always use the ones in GL_ARRAY_BUFFER)

    glDisableVertexAttribArray(0); //?
    glBindBuffer(GL_ARRAY_BUFFER, 0); //Unbind//*/

    /*GLuint bufferObject;
    // allocate memory ON the GPU
    glCreateBuffers(1, &bufferObject);
    glNamedBufferStorage(bufferObject, 8*8 * sizeof(float)*3, planePoints,
                                 GL_MAP_WRITE_BIT | GL_DYNAMIC_STORAGE_BIT);

    GLuint vao;
    // a construct that maps data from buffers into shaders
    glCreateVertexArrays(1, &vao);
    // enable the first attribute location in the shader
    glEnableVertexArrayAttrib(vao, 0);
    // buffer to index mapping
    glVertexArrayVertexBuffer(vao, 0, bufferObject, offset 0, sizeof(float)*3);
    // attribute to index mapping. can query shader for its attributes
    glVertexArrayAttribBinding(vao, glGetAttribLocation(shaderProgram, "vertexpos"),
    // buffer slot index 0);*/
  #endif

  // Pass control to GLUT for events
  glutMainLoop();

  return 0;
}
