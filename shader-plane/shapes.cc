
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

void getPlanePoints(int w, int h, float* points)
//float* getPlanePoints(int w, int h)
{
  //float points[w*h*6];
  //points = new float[w*h*6];
  bool direction=true;
  float wNorm = 1/(float)w;
  float hNorm = 1/(float)h;
  for (int j = 0; j < h; j++) {
    for (int i = 0; i < w; i++) {
      points[0]=(direction ? i : w-1-i)*wNorm;
      points[1]=j*hNorm;
      points[2]=0;

      points[3]=(direction ? i : w-1-i)*wNorm;
      points[4]=(j+1)*hNorm;
      points[5]=0;
      points += 6;
    }
    direction = !direction;
  }
  //return points;
}

void drawPlane(int w, int h)
{
  glPushMatrix();
  glTranslatef(-w/2., -h/2., 0);

  // https://www.khronos.org/opengl/wiki/Primitive
  glBegin(GL_TRIANGLE_STRIP);
  for (int j = 0; j < h; j++) {
    for (int i = 0; i < w; i++) {
      glVertex3f(i,j,0);
      glVertex3f(i,j+1,0);
    }
  }
  glEnd();

  glPopMatrix();
}

void drawAxis(){
  // https://www.khronos.org/opengl/wiki/Primitive
  glBegin(GL_LINES);
  glColor3f(1,0,0);
  glVertex3f(0,0,0);
  glVertex3f(1,0,0);

  glColor3f(0,1,0);
  glVertex3f(0,0,0);
  glVertex3f(0,1,0);

  glColor3f(0,0,1);
  glVertex3f(0,0,0);
  glVertex3f(0,0,1);
  glEnd();
}
