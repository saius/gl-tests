// https://www.khronos.org/opengl/wiki/Fragment_Shader
// gl_FragCoord gl_FrontFacing gl_PrimitiveID

// https://www.khronos.org/opengl/wiki/Uniform_(GLSL)
uniform int time;
//uniform float height;

varying vec4 pos;

void main (void)
{
  //gl_FragColor = vec4(gl_FragCoord.x*10., gl_FragCoord.y*0.1, 0., 1.);
  //gl_FragColor = vec4(pos.x/10.+gl_FragCoord.x/700., pos.y/10., 0., 1.);
  //gl_FragColor = vec4(1.,.5,.2,1.);
  float zCol = .5+pos.z;
  float xCol = .5+pos.x;
  float yCol = .5+pos.y;
  gl_FragColor = vec4(xCol*zCol,yCol*zCol,0,1.);
}
